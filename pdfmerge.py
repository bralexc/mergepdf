#!/usr/bin/env python
import sys
import datetime
import os
import time

import logging
# try:
#     from PyPDF2 import PdfFileReader, PdfFileWriter
# except ImportError:
#     from pyPdf import PdfFileReader, PdfFileWriter

import pypdf
from PIL import Image
from reportlab.lib.pagesizes import A4
from reportlab.lib.units import cm
from reportlab.pdfgen import canvas

logging.basicConfig(level=logging.INFO, format="%(message)s")

def pdf_cat(input_files, output_file):
    """Concatenates PDF files into one.

    Args:
        input_files (list): list of strings with the fullpaths to all pdf files to be merged
        output_stream (str): name of the output pdf file

    Returns:
        nothing

    """
    if sys.platform == "win32":
        import os, msvcrt    
        msvcrt.setmode(sys.stdout.fileno(), os.O_BINARY)

    with open(output_file, 'wb') as output_stream:
        input_streams = []
        try:
            # First open all the files, then produce the output file, and
            # finally close the input files. This is necessary because
            # the data isn't read from the input files until the write
            # operation. Thanks to
            # https://stackoverflow.com/questions/6773631/problem-with-closing-python-pypdf-writing-getting-a-valueerror-i-o-operation/6773733#6773733
            for input_file in input_files:
                input_streams.append(open(input_file, 'rb'))
            writer = pypdf.PdfWriter()
            # using 'False' in PdfFileReader because of problems with ID numbering in some pdfs
            for reader in (pypdf.PdfReader(i, False) for i in input_streams):
                for n in range(len(reader.pages)):
                    writer.add_page(reader.pages[n])
            writer.write(output_stream)
        finally:
            for f in input_streams:
                f.close()


def new_pdf_name(img_fname):
    basename, ext = os.path.splitext(img_fname)
    return f"{basename}.pdf"


def convert_img_to_a4_pdf(img_fname, pdf_fname):
    # Load image
    img = Image.open(img_fname)

    # A4 dimensions and margin
    a4_width, a4_height = A4
    margin = 1 * cm
    available_width = a4_width - (2 * margin)    # Margin on both sides
    # available_height = a4_height - margin        # Margin only at the top
    available_height = a4_height - (2 * margin)  # Margin at top and bottom

    # Resize image if larger than available space
    img_width, img_height = img.size
    if img_width > available_width or img_height > available_height:
        img.thumbnail((available_width, available_height), Image.LANCZOS)  # Updated resizing method

    # Get new image size after possible resizing
    img_width, img_height = img.size

    # Calculate image position (centralized)
    x = (a4_width - img_width) / 2
    y = a4_height - margin - img_height  # <margin> cm from top

    # Create a canvas and place the image
    c = canvas.Canvas(pdf_fname, pagesize=A4)
    c.drawImage(img_fname, x, y, width=img_width, height=img_height)
    c.showPage()
    c.save()


def main(argv):
    try:
        reorder_cmdline = True
        if '--noreorder' in argv:
            reorder_cmdline = False
            argv.remove('--noreorder')
        argv.remove('--reorder')

        pdf_files = []
        # if pdf list came from cmd line:
        if(len(argv) > 1):
            pdf_files = argv[1:]
            if reorder_cmdline:
                pdf_files.sort(key=lambda s: s.lower())

        # if not, ask for them now
        else:
            while True:
                tmp = input('Drag pdf file here or ENTER to end: ')
                # if multiple files were drag'n'dropped into window, split them
                # before adding to the list, or we'll get an error afterwards
                if tmp.count('"') > 2:
                    tmp = [i.replace('"','') for i in tmp.split('""')]
                    pdf_files.extend(tmp)
                else:
                    tmp = tmp.replace('"','')
                    if len(tmp) < 2:
                        break
                    pdf_files.append(tmp)

        # check if all files are indeed pdf, remove the ones that aren't
        for pdf in pdf_files:
            if not pdf.lower().endswith('pdf'):
                try:
                    # might be an image, so let's try to convert it
                    pdf_fname = new_pdf_name(pdf)
                    convert_img_to_a4_pdf(pdf, pdf_fname)
                    # if we're successfull, let's replace the img name in the 
                    #  list with the new pdf name
                    pdf_files[pdf_files.index(pdf)] = pdf_fname
                except Exception as e:
                    logging.error(f"Error processing file: {pdf}")
                    logging.error(e)
                    logging.error('Removing from list.')
                    pdf_files.remove(pdf)

        # outpuf file will be named pdfpath/YYMMDDHHMMSS_file1_merged.pdf, with
        # pdfpath = path of the first pdf
        curtime = datetime.datetime.now().strftime("%y%m%d_%H%M%S")
        curdir = os.path.split(pdf_files[0])[0]
        # fname1 = os.path.splitext(os.path.basename(pdf_files[0]))[0].replace(" ", "")

        outfile = os.path.join(
            curdir,
            # f"{curtime}_{fname1}_merged.pdf"
            f"{curtime}_merged.pdf"
        )

        # now actually merge pdf files
        if len(pdf_files) > 0:
            logging.info('Merging %i files:' % len(pdf_files))
            for pdf_file in pdf_files:
                logging.info(f" - {pdf_file}")
            pdf_cat(pdf_files, outfile)
        
        logging.info(f"Done.\nExported '{outfile}' to\n'{curdir}'.")
        time.sleep(1)
        
    except Exception as e:
        logging.error('\nError(s) found in the process:')
        logging.error(e)
        logging.error("Press ENTER to exit.")


if __name__ == '__main__':
    main(sys.argv)
    input()