# Merge PDF

Utility to easily merge (or, more maybe approppriately, append) several PDF files into one.

## Usage

### CLI

Call the script with a list of files to be merged:

```bash
pdfmerge.py "File 1.pdf" File2.pdf "File3.PDF"
```

Files are merged in the order they are provided.

### Drag-n-Drop

Create a Shortcut to `pdfmerge.bat`. Select files and drag-n-drop them over the shortcut.

Files are merged alphabetically.

### Windows Send-To Menu

1. Use `Win+R` to open the Execute dialog and run `shell:sendto`
2. Place a shortcut to `pdfmerge.bat` in this folder, rename it to `Merge PDF`
3. Select PDF files, right-click and choose **Send to > Merge PDF**

Files are merged alphabetically.

## Output

In all cases, the output PDF will be named `YYDDMMHHMM_File1.pdf`, placed in the same folder as the first file.

## Requirements

* Python 3.7+
* `pypdf`
* `pillow`
* `reportlab`
