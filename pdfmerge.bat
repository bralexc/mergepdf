@echo off
rem '%~dp0' points to running directory, so 'merge.pdf' is created at calling dir location
mamba activate py311 && python.exe %~dp0pdfmerge.py --reorder %*
pause